//var gulp = require('gulp');

var { src, dest,series,parallel,watch} = require('gulp');
var gulp = require('gulp');
var concat = require('gulp-concat');
const babel = require('gulp-babel');
const sass = require('gulp-sass')(require('sass'));
var clean = require('gulp-rimraf');
sass.compiler = require('node-sass');
const browserSync = require('browser-sync').create();
const origin = 'src';
const destination = 'dist';





 async function cl() {
  console.log("Clean all files in build folder");
  await dest(`${destination}`).pipe(clean());
  
}


function html(cb){
    src(`${origin}/**/*.html`).pipe(dest('dist'));
    cb();
}


// function scss(cb){
//   src(`${origin}/scss/main.scss`)
//   .pipe(sass(
//     {
//       outputStyle: 'compressed'
//     }
//   ))
//   .pipe(dest(`${destination}/css`))
//   cb();
// }

function scss(cb) {
  // return gulp.
  //   
  //   .pipe(gulp.dest(`${origin}/css`));
  src(`${origin}/scss/**/*.scss`)
  //.pipe(sass().on('error', sass.logError))
  .pipe(sass.sync().on('error', sass.logError))
  .pipe(dest(`${destination}/css`));
  cb();
};

function img(cb){
  src('src/img/*.png')
  .pipe(dest('dist/img'));
  cb();
}
function faScss(cb) {
  // return gulp.
  //   
  //   .pipe(gulp.dest(`${origin}/css`));
  src(`${origin}/fa-scss/**/*.scss`)
  //.pipe(sass().on('error', sass.logError))
  .pipe(sass.sync().on('error', sass.logError))
  
  .pipe(dest(`${destination}/fa-css/`));
  cb();
};

function  webFont(cb){
  src([`${origin}/webfonts/*.ttf`, 
  `${origin}/webfonts/*.woff2`
])
.pipe(dest(`${destination}/webfonts/`));
  cb();
}

function js(cb){
  src([
    `${origin}/js/nav.js`, 
    `${origin}/js/modal.js`
    
])
.pipe(babel({
  presets: ['@babel/env']
}))
.pipe(concat('script.js'))
.pipe(dest(`${destination}/js`))
  cb();
}

function watcher(cb){
  watch(`${origin}/**/*.html`).on('change', series(html,browserSync.reload));
  watch(`${origin}/scss/**/*.scss`).on('change', series(scss,browserSync.reload));
  watch(`${origin}/fa-css/**/*.scss`).on('change', series(faScss,browserSync.reload));
  watch(`${origin}/webfonts/*`).on('change', series(webFont,browserSync.reload));
  watch(`${origin}/**/*.js`).on('change', series(js,browserSync.reload));
  watch(`${origin}/img/*.png`).on('change', series(img,browserSync.reload));
  cb();
}

function server(cb){
  browserSync.init({
    open:false,
    notify: false,
    server:{
      baseDir: destination
    }
  }
  );
cb();
}



 exports.default = series(cl,parallel(html,scss,faScss,js,img,watcher,webFont),server);
